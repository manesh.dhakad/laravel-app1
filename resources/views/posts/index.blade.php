@extends('posts.layout')

 

@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2> Post Management</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ route('posts.create') }}"> Create New Post</a>

            </div>

        </div>

    </div>

   

    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
 
    <table class="table table-bordered">

        <tr>

            <th>No</th>

            <th>Title</th>
            <th>Category</th> 
            <th>Status</th>

            <th width="280px">Action</th>

        </tr>

        @foreach ($posts as $data) 

        <tr>

            <td>{{ ++$i }}</td>

            <td>{{ $data->title }}</td>
            <td>{{ $data->category->title }}</td>

            <td>{{ $data->status }}</td>

            <td>

                <form action="{{ route('posts.destroy',$data->id) }}" method="POST">

   

                    <a class="btn btn-info" href="{{ route('posts.show',$data->id) }}">Show</a>

    

                    <a class="btn btn-primary" href="{{ route('posts.edit',$data->id) }}">Edit</a>

   

                    @csrf

                    @method('DELETE')

      

                    <button type="submit" class="btn btn-danger">Delete</button>

                </form>

            </td>

        </tr>

        @endforeach

    </table>

  

    {!! $posts->links() !!}

      

@endsection