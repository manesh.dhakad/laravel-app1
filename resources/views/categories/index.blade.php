@extends('categories.layout')

 

@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2> Category Management</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ route('categories.create') }}"> Create New Category</a>

            </div>

        </div>

    </div>

   

    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
 

   <!--  <div class="mt-1 mb-4">
        <div class="relative max-w-xs">
            <form action="{{ route('categories.index') }}" method="GET">
                <label for="search" class="sr-only">
                    Search
                </label>
                <input type="text" name="s"
                    class="block w-full p-3 pl-10 text-sm border-gray-200 rounded-md focus:border-blue-500 focus:ring-blue-500 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400"
                    placeholder="Search..." />
                <div class="absolute inset-y-0 left-0 flex items-center pl-4 pointer-events-none">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 text-gray-400" fill="none"
                        viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                    </svg>
                </div>
            </form>
        </div>

    </div> -->


    <table class="table table-bordered">

        <tr>

            <th>No</th>

            <th>Title</th>

            <th>Status</th>

            <th width="280px">Action</th>

        </tr>

        @foreach ($categories as $data)

        <tr>

            <td>{{ ++$i }}</td>

            <td>{{ $data->title }}</td>

            <td>{{ $data->status }}</td>

            <td>
                

                <form action="{{ route('categories.destroy',$data->id) }}" method="POST">

   

                    <a class="btn btn-info" href="{{ route('categories.show',$data->id) }}">Show</a>

    

                    <a class="btn btn-primary" href="{{ route('categories.edit',$data->id) }}">Edit</a>

   

                    @csrf

                    @method('DELETE')

      

                    <button  type="submit" class="btn btn-danger">Delete</button>

                </form>

            </td>

        </tr>

        @endforeach

    </table>
 

    {!! $categories->links() !!}

      

@endsection