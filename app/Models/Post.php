<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Post extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [ 
        'title', 
        'category_id', 
        'description' 
    ];  

    public function category(){
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

}
