<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('trashed')) {
            $posts = Post::onlyTrashed()
                ->latest()->paginate(10);
        } else {
            $posts = Post::latest()->paginate(10);
        } 
        return view('posts.index',compact('posts')) 
            ->with('i', (request()->input('page', 1) - 1) * 5); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::withoutTrashed()->get()->pluck('title','id'); 
        $selected = '';
        return view('posts.create',compact('categories','selected'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'title' => 'required',  
            'category_id' => 'required',   
            'description' => 'required', 
        ]); 
       // dd($request->title); 
        Post::create($request->all());   
        return redirect()->route('posts.index') 
                        ->with('success','Post created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    { 
        $categories = Category::withoutTrashed()->get()->pluck('title','id'); 
        $selected = $post->category_id;
 
        return view('posts.edit',compact('post','categories','selected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    { 
        $request->validate([ 
            'title' => 'required',  
            'category_id' => 'required',  
            'description' => 'required', 
        ]);  
        $postData = [
            'title' => $request->title,
            'category_id' => $request->category_id,
            'description' => $request->description
        ]; 
 
        $post->update($postData); 
        return redirect()->route('posts.index') 
                        ->with('success','Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {  
        $post->delete();  
        return redirect()->route('posts.index') 
                        ->with('success','Post deleted successfully');
    }
}
